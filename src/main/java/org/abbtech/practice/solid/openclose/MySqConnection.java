package org.abbtech.practice.solid.openclose;

public class MySqConnection implements ConnectionService{
    @Override
    public void getConnection() {
        System.out.println("get MySql Connection");
    }
}
