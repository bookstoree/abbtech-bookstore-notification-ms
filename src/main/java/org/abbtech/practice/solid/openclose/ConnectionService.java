package org.abbtech.practice.solid.openclose;

public interface ConnectionService {

    void getConnection();
}
