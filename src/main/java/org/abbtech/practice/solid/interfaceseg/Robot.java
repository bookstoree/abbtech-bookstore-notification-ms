package org.abbtech.practice.solid.interfaceseg;

public class Robot implements Workable {
    @Override
    public void work() {
        System.out.println("Human is working");

    }
}
