package org.abbtech.practice.solid.interfaceseg;

public interface Workable {
    void work();
}
