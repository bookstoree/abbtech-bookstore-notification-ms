package org.abbtech.practice.solid.liskov;

import org.abbtech.practice.exception.GeneralTechException;
import org.abbtech.practice.exception.enums.GeneralExceptionEnum;

public class Penguin extends Bird {
    @Override
    public void fly() {
        System.out.println("Penguin can not fly");
    }
}
