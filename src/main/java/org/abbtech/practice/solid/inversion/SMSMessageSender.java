package org.abbtech.practice.solid.inversion;

public class SMSMessageSender implements MessageSender{
    @Override
    public void sendMessage() {
        System.out.println("Send SMS message");
    }
}
