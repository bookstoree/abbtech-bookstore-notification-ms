package org.abbtech.practice.solid.inversion;

public interface NotificationService {
    void sendNotification();
}
