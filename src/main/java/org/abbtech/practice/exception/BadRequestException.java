package org.abbtech.practice.exception;

import lombok.Getter;
import org.abbtech.practice.exception.enums.BadRequestExceptionEnum;

@Getter
public class BadRequestException extends RuntimeException {
    private final BadRequestExceptionEnum badRequestExceptionEnum;

    public BadRequestException(BadRequestExceptionEnum badRequestExceptionEnum) {
        super(badRequestExceptionEnum.toString());
        this.badRequestExceptionEnum = badRequestExceptionEnum;
    }

    public BadRequestException(BadRequestExceptionEnum badRequestExceptionEnum, Throwable throwable) {
        super(badRequestExceptionEnum.toString(), throwable);
        this.badRequestExceptionEnum = badRequestExceptionEnum;
    }
}
