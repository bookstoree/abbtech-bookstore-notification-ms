package org.abbtech.practice.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.UUID;

@Entity
@Getter
@Setter
public class Notification {
    @Id
    @GeneratedValue
    private UUID id;
    private String message;
    private String userEmail;
    private UUID userId;
    private UUID orderId;

}
